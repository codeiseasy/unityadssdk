package com.codeiseasy.unityads

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.widget.Switch
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.codeiseasy.unityads.pref.UnityAdsPrefs
import com.codeiseasy.unityads.receiver.NetworkStateReceiver
import com.codeiseasy.unityads.receiver.NetworkStateReceiverCallback
import java.util.*

class MainActivity : AppCompatActivity(), NetworkStateReceiverCallback {
    private var networkStateReceiver: NetworkStateReceiver? = null
    private var unityAdsPrefs: UnityAdsPrefs? = null

    override fun onReceiver(context: Context?, intent: Intent?) {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        networkStateReceiver = NetworkStateReceiver(this)
        networkStateReceiver?.register(this)
        unityAdsPrefs = UnityAdsPrefs(this)
    }



    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.options_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
           R.id.item_unity_settings -> dialogSettings()

            R.id.item_unity_banner -> {}

            R.id.item_unity_interstitial -> { }

            R.id.item_unity_rewarded -> { }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun dialogSettings() {
        val rootView = LayoutInflater.from(this).inflate(R.layout.fb_ad_settings, null)
        val switchEnableBanner = rootView.findViewById<Switch>(R.id.switch_enable_banner)
        val switchEnableInterstitial = rootView.findViewById<Switch>(R.id.switch_enable_interstitial)
        val switchEnableRewarded = rootView.findViewById<Switch>(R.id.switch_enable_rewarded)

        switchEnableBanner.isChecked = unityAdsPrefs!!.readBoolean("key_banner", true)
        switchEnableInterstitial.isChecked = unityAdsPrefs!!.readBoolean("key_interstitial", false)
        switchEnableRewarded.isChecked = unityAdsPrefs!!.readBoolean("key_rewarded", false)

        AlertDialog.Builder(this)
            .setTitle(resources.getString(R.string.ad_settings_label))
            .setView(rootView)
            .setPositiveButton(resources.getString(R.string.btn_apply)) { dialog, _ ->
                dialog.dismiss()
                unityAdsPrefs?.writeBoolean("key_banner", switchEnableBanner.isChecked)
                unityAdsPrefs?.writeBoolean("key_interstitial", switchEnableInterstitial.isChecked)
                unityAdsPrefs?.writeBoolean("key_rewarded", switchEnableRewarded.isChecked)
                recreate()
            }
            .setNegativeButton(resources.getString(R.string.btn_cancel), null)
            .show()
    }

    override fun onBackPressed() {
        //startActivity(Intent(this, PagerActivity::class.java))
        finishAffinity()
    }

    override fun onDestroy() {
        super.onDestroy()
        networkStateReceiver?.unregister()
    }

    private fun getDeviceId() {
        val tm: TelephonyManager = getSystemService(TELEPHONY_SERVICE) as TelephonyManager
        val tmDevice: String
        val tmSerial: String
        tmDevice = "" + tm.deviceId
        tmSerial = "" + tm.simSerialNumber
        val androidId: String = "" + Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)

        val deviceUuid = UUID(androidId.hashCode().toLong(), tmDevice.hashCode().toLong() shl 32 or tmSerial.hashCode().toLong())
        val deviceId: String = deviceUuid.toString()
        Log.d("DEVICE_ID_INFO", deviceId)
    }
}