package com.codeiseasy.unityads

object Constants {
    @JvmStatic
    val UNITY_GAME_ID = "3924581"

    @JvmStatic
    val UNITY_BANNER_PLACEMENT_ID = "banner"

    @JvmStatic
    val UNITY_INTERSTISIAL_VIDEO_PLACEMENT_ID = "video"

    @JvmStatic
    val UNITY_REWARDED_VIDEO_PLACEMENT_ID = "rewardedVideo"

    const val ENABLE_ADS = true
    const val ENABLE_BANNER = true
    const val ENABLE_INTERSTITIAL_VIDEO = false
    const val ENABLE_REWARDED_VIDEO = true
}